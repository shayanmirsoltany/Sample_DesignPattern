﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample
{
    public interface INotife
    {
        string Send();
    }
}
