﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample
{
    public class EmailSender : INotife
    {
        public EmailSender(string Name)
        {
            this.Name = Name;
        }
        public string Name { get; set; }
        public string Message { get; set; }
        public string Send()
        {
            Message = Name+" Email sended";
            return Message;
        }
    }
    public class SMSSender : INotife
    {
        public SMSSender(string Name)
        {
            this.Name = Name;
        }
        public string Name { get; set; }
        public string Message { get; set; }
        public string Send()
        {
            Message = Name + " SMS sended";
            return Message;
        }
    }
    public class Add
    {
        INotife e;
        public Add(INotife not)
        {
            e = not;
        }
        public string getMeesage()
        {
           return e.Send();
        }
    }

}
