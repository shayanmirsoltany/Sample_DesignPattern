﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample.Solid.L
{
    public interface ICrud
    {
        void Add();
    }
    public class LiskovSubstitution
    {
        public int Num { get; set; }
        public string Name { get; set; }
    }
    public class Product10 : LiskovSubstitution, ICrud
    {
        public void Add()
        {
            
        }
    }
    public class Product20 : LiskovSubstitution//in class niazi be method Add nadare va faghat property haie iek class ro mikhad
    {

    }
}
