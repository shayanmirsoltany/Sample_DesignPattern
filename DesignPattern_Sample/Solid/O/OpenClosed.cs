﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample.Solid.O
{
    public class OpenClosed
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public virtual double GetPrice()
        {
            return 0;
        }
    }
    public class Product1 : OpenClosed
    {
        public override double GetPrice()
        {
            return Price * 10;
        }
    }
    public class Product2 : OpenClosed
    {
        public override double GetPrice()
        {
            return Price * 20;
        }
    }
    public class Product3 : OpenClosed
    {
        public override double GetPrice()
        {
            return Price * 30;
        }
    }
}
