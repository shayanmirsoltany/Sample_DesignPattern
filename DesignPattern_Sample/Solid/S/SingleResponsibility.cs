﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample.Solid.S
{
    //har kari dar kelase khod anjam mishavad.
    public class SingleResponsibility
    {
        public int Num { get; set; }
        public string Name { get; set; }
    }
    public class Crud
    {
        public void Add(SingleResponsibility s)
        {
            var logger = new FileLog();
            try
            {

            }
            catch (Exception ex)
            {
                FileLog.Log(ex.ToString());
            }
        }
    }

    public class FileLog
    {
        public static void Log(string ex)
        {

        }
    }
}
