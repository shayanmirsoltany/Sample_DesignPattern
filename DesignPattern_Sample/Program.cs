﻿using DesignPattern_Sample.Solid.L;
using DesignPattern_Sample.Solid.O;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPattern_Sample
{
    class Program
    {
        static void Main(string[] args)
        {
            //////////Solid-Open Closed Principle
            //var x = new Product1();
            //x.Price = 200;
            //var y = new Product2();
            //y.Price = 200;
            //var z = new Product3();
            //z.Price = 200;
            //Console.WriteLine(x.GetPrice());
            //Console.WriteLine(y.GetPrice());
            //Console.WriteLine(z.GetPrice());
            //Console.ReadLine();

            ///////////Solid-Liskov
            //var x = new Product10();
            //var y = new Product20();
            //x.Add();

            //////////Solid-Dependency Inversion Principle
            //var e = new EmailSender("shayan");
            //var s = new SMSSender("Ahmad");
            //var f = new Add(e);
            //var sss = new Add(s);
            //Console.WriteLine(f.getMeesage());
            //Console.WriteLine(sss.getMeesage());
            //Console.ReadLine();
        }
    }
}
